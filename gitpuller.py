#!/usr/bin/env python3

#required libraries for this project
import requests
from bs4 import BeautifulSoup
from colorama import Fore
import sys
import os
import errno


#This function defines the banner for the project
def banner():
	text = '''
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n
	+     Gitpuller is a tool used to find and pull the .git repo        +\n
	+     Use this tool at your own risk                                 +\n
	+     Usage might be illegal in certain circumstances                +\n
	+     Designed for educational purposes only                         +\n
	+     Author: @crypto_grapper_                                       +\n
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'''
	print(Fore.GREEN + text, "\n")


def usage():
	print(Fore.WHITE + "Usage: python3 gitpuller.py https://domain.com/ repository")

if (len(sys.argv)<=1):
	banner()
	usage()

elif len(sys.argv) == 3:
	banner()
	print(Fore.WHITE)
	directory = sys.argv[2]
	try:
		print(Fore.GREEN + "[+] Creating", directory+"/.git/")
		os.makedirs(directory)
		os.chdir(directory)
		os.mkdir(".git")
		print(Fore.WHITE)
		url = sys.argv[1]+"/.git/"
		headers = {'Accept-Encoding': 'gzip'}
		response = requests.get(url, headers=headers, allow_redirects=False)
		print(url + " <------- [ {} ]".format(response.status_code))

		soup = BeautifulSoup(response.text, "html.parser")
		urls = []
		for link in soup.find_all('a'):
			print(link)

	except OSError as e:
		if e.errno == errno.EEXIST:
			print(Fore.RED + "Folder already exists\n")
		else:
			raise
	
	

"""if (len(sys.argv) == 2):
	url = sys.argv[1]+"/.git/"
	headers = {'Accept-Encoding': 'gzip'}
	response = requests.get(url, headers=headers, allow_redirects=False)
	print(url + " <------- [ {} ]".format(response.status_code))

if len(sys.argv) == 3:
	directory = sys.argv[2]
	print(directory)"""

'''txt = response.text

# Create the document
doc = txt

# Initialize the object with the document
soup = BeautifulSoup(doc, "html.parser")

# Get the whole body tag
tag = soup.body

# Print each string recursively
for string in tag.strings:
	print(string)
'''